import java.util.Scanner;

public class ExerciciDom {
	static Scanner reader = new Scanner(System.in);

	/**
	 * Declaracio del main
	 * @author Noelia Castillo
	 * @version IES Sabadell, 10/03/2019
	 */
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
	//declaracio d'array i variable
		int[] n = new int[8];
		int res = 0;
		
		
		while(n[0] != -1) {
			omplirTaula(n);
			if (n[0] != -1)	{
				res = sumaTaula(n);
				
				System.out.println(res);
			}			
		}
	}

	/**declaracio del omplirTaula
	 * Metode per recaptar les donacions.
	 * @param n Array a omplir.
	 * 
	 */
	public static void omplirTaula(int[] n) {
		int i = 0;
		while(n[0] != -1 && i < n.length ) {
			n[i] = reader.nextInt();
			i++;
			
		}
	}

	/**declaracio del sumTaula
	 * Metode per sumar totes les donacions de l'array 'n'.
	 * @param n Array a omplir.
	 * @return el resultat en int de l'array.
	 * 
	 */
	
	public static int sumaTaula(int[] n) {
		int res= 0;
		int i = 0, j = 0;
		for (i = 0; i < n.length; i++) {
			res = res + n[i];
		}
		return res;
	}
}
